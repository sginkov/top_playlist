source 'https://rubygems.org'

# Bundle edge Rails instead: gem 'rails', github: 'rails/rails'
gem 'rails', '4.0.5'

# Use mysql as the database for Active Record
gem 'mongoid'

# Build JSON APIs with ease. Read more: https://github.com/rails/jbuilder
gem 'jbuilder', '~> 1.2'

gem 'devise'
gem 'activeadmin', github: 'gregbell/active_admin'

# Activeadmin Mongoid Blog
gem 'mongoid_slug'
gem 'mongoid_search'
gem 'nokogiri'
gem 'select2-rails'
gem 'ransack', github: 'activerecord-hackery/ransack'

# Use SCSS for stylesheets
gem 'sass-rails', '~> 4.0.2'

# Use Uglifier as compressor for JavaScript assets
gem 'uglifier', '>= 1.3.0'

# Use CoffeeScript for .js.coffee assets and views
gem 'coffee-rails', '~> 4.0.0'

# Use jquery as the JavaScript library
gem 'jquery-rails'

# Bootstrap styles
gem 'therubyracer'
gem 'bootstrap-sass'

gem 'country_select'
gem 'rails_config'

gem 'wombat'

gem 'carrierwave-mongoid', require: 'carrierwave/mongoid'
gem 'mini_magick'
gem 'aws-s3'
gem 'fog'

gem 'state_machine'

gem 'elasticsearch-model', git: 'git://github.com/elasticsearch/elasticsearch-rails.git'
gem 'elasticsearch-rails', git: 'git://github.com/elasticsearch/elasticsearch-rails.git'
gem 'will_paginate', '~> 3.0.6'
gem 'will_paginate_mongoid'
gem 'sidekiq'
gem 'sinatra', '>= 1.3.0', require: nil
gem 'whenever', require: false

group :doc do
  # bundle exec rake doc:rails generates the API under doc/api.
  gem 'sdoc', require: false
end

group :development, :test do
  gem 'foreman'
  gem 'capybara'
  gem 'rspec-rails'
end

group :test do
  gem 'database_cleaner'
  gem 'shoulda-matchers', require: false
  gem 'rspec-activemodel-mocks'
  gem 'factory_girl_rails', '~> 4.0'
end

group :production do
  gem 'unicorn'
end

gem 'rollbar', '~> 1.3.0'
gem 'hipchat'
gem 'newrelic_rpm'
gem 'grape'
gem 'grape-swagger'
gem 'swagger-ui_rails'
gem 'rack-cors', require: 'rack/cors'
gem 'draper', '~> 1.3'
gem 'ckeditor'
gem 'mongoid-slug'
gem 'vkontakte_api', '~> 1.4'

