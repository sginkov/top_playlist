admins = [{ email: 's.ginkov@gmail.com', password: '12345678', password_confirmation: '12345678' }]
admins.each do |admin_attrs|
  AdminUser.find_or_initialize_by(email: admin_attrs[:email]).update(admin_attrs)
end

default_sources = [
  {
    name: 'Billboard',
    link: 'http://www.billboard.com/charts/hot-100',
    country: 'US',
    tracks_count: 100,
    change_period: 'week',
    genre: 'pop',
    crawler: :billboard
  },
  {
    name: 'Муз ТВ',
    link: 'http://muz-tv.ru/charts/results',
    country: 'RU',
    tracks_count: 20,
    change_period: 'week',
    genre: 'pop',
    crawler: :muz_tv
  },
  {
    name: 'Europa Plus',
    link: 'http://www.europaplus.ru/?go=chart40',
    country: 'RU',
    tracks_count: 40,
    change_period: 'week',
    genre: 'pop',
    crawler: :europa_plus
  },
  {
    name: 'MTV Russia',
    link: 'http://www.mtv.ru/music/charts/top20',
    country: 'RU',
    tracks_count: 20,
    change_period: 'week',
    genre: 'pop',
    crawler: :mtv
  },
  {
    name: 'MTV UK',
    link: 'http://www.mtv.co.uk/music/charts/this-weeks-top-20',
    country: 'GB',
    tracks_count: 20,
    change_period: 'week',
    genre: 'pop',
    crawler: :mtv
  },
  {
    name: 'MTV Germany',
    link: 'http://www.mtv.de/charts/5-hitlist-germany-top-100',
    country: 'DE',
    tracks_count: 100,
    change_period: 'week',
    genre: 'pop',
    crawler: :mtv_germany
  },
  {
    name: 'MTV Asia',
    link: 'http://www.mtvasia.com/mini/chartattack/',
    region: 'Asia',
    tracks_count: 20,
    change_period: 'week',
    genre: 'pop',
    crawler: :mtv_asia
  },
  {
    name: 'Los 40 Principales',
    link: 'http://los40.com/lista40/',
    country: 'ES',
    tracks_count: 40,
    change_period: 'week',
    genre: 'pop',
    crawler: :los40
  },
  {
    name: 'Radio-Charts Deutschland',
    link: 'http://www.radiocharts.com/html/charts_de_main.htm',
    country: 'DE',
    tracks_count: 100,
    change_period: 'week',
    genre: 'pop',
    crawler: :deutschland_radiocharts
  },
  {
    name: 'Big Top 40',
    link: 'http://www.bigtop40.com/chart/archive/2014/',
    country: 'GB',
    tracks_count: 40,
    change_period: 'week',
    genre: 'pop',
    crawler: :big_top_40
  },
  {
    name: 'AT 40',
    link: 'http://www.at40.com/top-40',
    country: 'US',
    tracks_count: 40,
    change_period: 'week',
    genre: 'pop',
    crawler: :at40
  },
  {
    name: 'Last FM',
    link: 'http://www.last.fm/charts/tracks/top/place/all',
    tracks_count: 20,
    change_period: 'week',
    genre: 'pop',
    crawler: :last_fm
  },
  {
    name: 'BBC Radio',
    link: 'http://www.bbc.co.uk/radio1/chart/singles',
    country: 'GB',
    tracks_count: 40,
    change_period: 'week',
    genre: 'pop',
    crawler: :bbc_radio
  },
  {
    name: 'BBC Radio Dance',
    link: 'http://www.bbc.co.uk/radio1/chart/dancesingles',
    country: 'GB',
    tracks_count: 40,
    change_period: 'week',
    genre: 'dance',
    crawler: :bbc_radio
  },
  {
    name: 'BBC Radio RnB',
    link: 'http://www.bbc.co.uk/radio1/chart/rnbsingles',
    country: 'GB',
    tracks_count: 40,
    change_period: 'week',
    genre: 'rnd_hiphop',
    crawler: :bbc_radio
  },
  {
    name: 'BBC Radio Rock',
    link: 'http://www.bbc.co.uk/radio1/chart/rocksingles',
    country: 'GB',
    tracks_count: 40,
    change_period: 'week',
    genre: 'rock',
    crawler: :bbc_radio
  },
  {
    name: 'BBC Radio Indie',
    link: 'http://www.bbc.co.uk/radio1/chart/indiesingles',
    country: 'GB',
    tracks_count: 30,
    change_period: 'week',
    genre: 'indie',
    crawler: :bbc_radio
  },
  {
    name: 'Aria Charts',
    link: 'http://www.ariacharts.com.au/chart/singles',
    country: 'AU',
    tracks_count: 50,
    change_period: 'week',
    genre: 'pop',
    crawler: :aria_charts
  },
  {
    name: 'France Radio Airplay Top 300',
    link: 'http://www.charly1300.com/topfrance.htm',
    country: 'FR',
    tracks_count: 300,
    change_period: 'week',
    genre: 'pop',
    crawler: :france_radio_airplay
  },
  {
    name: 'iTunes',
    link: 'https://www.apple.com/itunes/charts/songs/',
    country: 'US',
    tracks_count: 100,
    change_period: 'week',
    genre: 'pop',
    crawler: :itunes
  },
  {
    name: 'Billboard R&B/HIP-HOP',
    link: 'http://www.billboard.com/charts/r-b-hip-hop-songs',
    country: 'US',
    tracks_count: 25,
    change_period: 'week',
    genre: 'rnd_hiphop',
    crawler: :billboard
  },
  {
    name: 'Billboard ROCK',
    link: 'http://www.billboard.com/charts/rock-songs',
    country: 'US',
    tracks_count: 25,
    change_period: 'week',
    genre: 'rock',
    crawler: :billboard
  },
  {
    name: 'Billboard DANCE CLUB',
    link: 'http://www.billboard.com/charts/dance-club-play-songs',
    country: 'US',
    tracks_count: 25,
    change_period: 'week',
    genre: 'dance',
    crawler: :billboard
  },
  {
    name: 'Billboard DANCE/ELECTRONIC',
    link: 'http://www.billboard.com/charts/dance-electronic-songs',
    country: 'US',
    tracks_count: 25,
    change_period: 'week',
    genre: 'dance',
    crawler: :billboard
  },
  {
    name: 'Billboard Canada',
    link: 'http://www.billboard.com/charts/canadian-hot-100',
    country: 'CA',
    tracks_count: 50,
    change_period: 'week',
    genre: 'pop',
    crawler: :billboard
  },
  {
    name: 'Billboard Mexico',
    link: 'http://www.billboard.com/charts/regional-mexican-songs',
    country: 'MX',
    tracks_count: 20,
    change_period: 'week',
    genre: 'pop',
    crawler: :billboard
  },
  {
    name: 'Shazam USA',
    link: 'http://www.shazam.com/charts/us_top_100',
    country: 'US',
    tracks_count: 100,
    change_period: 'week',
    genre: 'pop',
    crawler: :shazam
  },
  {
    name: 'Shazam Brazil',
    link: 'http://www.shazam.com/charts/br_top_100',
    country: 'BR',
    tracks_count: 100,
    change_period: 'week',
    genre: 'pop',
    crawler: :shazam
  },
  {
    name: 'Shazam China',
    link: 'http://www.shazam.com/charts/cn_top_100',
    country: 'CN',
    tracks_count: 100,
    change_period: 'week',
    genre: 'pop',
    crawler: :shazam
  },
  {
    name: 'Shazam France',
    link: 'http://www.shazam.com/charts/fr_top_100',
    country: 'FR',
    tracks_count: 100,
    change_period: 'week',
    genre: 'pop',
    crawler: :shazam
  },
  {
    name: 'Shazam Germany',
    link: 'http://www.shazam.com/charts/de_top_100',
    country: 'DE',
    tracks_count: 100,
    change_period: 'week',
    genre: 'pop',
    crawler: :shazam
  },
  {
    name: 'Shazam Italy',
    link: 'http://www.shazam.com/charts/it_top_100',
    country: 'IT',
    tracks_count: 100,
    change_period: 'week',
    genre: 'pop',
    crawler: :shazam
  },
  {
    name: 'Shazam',
    link: 'http://www.shazam.com/charts/web_chart_global',
    tracks_count: 100,
    change_period: 'week',
    genre: 'pop',
    crawler: :shazam
  },
  {
    name: 'Shazam Hip-Hop',
    link: 'http://www.shazam.com/charts/genre/hip-hop',
    tracks_count: 10,
    change_period: 'week',
    genre: 'rnd_hiphop',
    crawler: :shazam
  },
  {
    name: 'Shazam Dance',
    link: 'http://www.shazam.com/charts/genre/dance',
    tracks_count: 10,
    change_period: 'week',
    genre: 'dance',
    crawler: :shazam
  },
  {
    name: 'Radio Record Superchart',
    link: 'http://www.radiorecord.ru/radio/charts',
    tracks_count: 25,
    change_period: 'week',
    country: 'RU',
    genre: 'dance',
    crawler: :radio_record
  },
  {
    name: 'Radio Energy',
    link: 'http://www.energyfm.ru/?an=nrj_hot30',
    tracks_count: 30,
    change_period: 'week',
    country: 'RU',
    genre: 'pop',
    crawler: :radio_energy
  },
  {
    name: 'BIG LOVE 20',
    link: 'http://www.loveradio.ru/biglove_chart.htm',
    tracks_count: 20,
    change_period: 'week',
    country: 'RU',
    genre: 'pop',
    crawler: :love_radio
  },
  {
    name: 'Radio Record Top 100',
    link: 'http://www.radiorecord.ru/radio/top100/detail-flat.php?station=rr',
    tracks_count: 100,
    change_period: 'week',
    country: 'RU',
    genre: 'dance',
    crawler: :radio_record_top100
  },
  {
    name: 'Russian Mix Top 100',
    link: 'http://www.radiorecord.ru/radio/top100/detail-flat.php?station=rus',
    tracks_count: 100,
    change_period: 'week',
    country: 'RU',
    genre: 'dance',
    crawler: :radio_record_top100
  },
  {
    name: 'Record Club Top 100',
    link: 'http://www.radiorecord.ru/radio/top100/detail-flat.php?station=club',
    tracks_count: 100,
    change_period: 'week',
    country: 'RU',
    genre: 'dance',
    crawler: :radio_record_top100
  },
  {
    name: 'Record Dancecore Top 100',
    link: 'http://www.radiorecord.ru/radio/top100/detail-flat.php?station=dc',
    tracks_count: 100,
    change_period: 'week',
    country: 'RU',
    genre: 'dance',
    crawler: :radio_record_top100
  },
  {
    name: 'Record Hardstyle Top 100',
    link: 'http://www.radiorecord.ru/radio/top100/detail-flat.php?station=teo',
    tracks_count: 100,
    change_period: 'week',
    country: 'RU',
    genre: 'dance',
    crawler: :radio_record_top100
  },
  {
    name: 'Record Deep Top 100',
    link: 'http://www.radiorecord.ru/radio/top100/detail-flat.php?station=deep',
    tracks_count: 100,
    change_period: 'week',
    country: 'RU',
    genre: 'dance',
    crawler: :radio_record_top100
  },
  {
    name: 'Record Dubstep Top 100',
    link: 'http://www.radiorecord.ru/radio/top100/detail-flat.php?station=dub',
    tracks_count: 100,
    change_period: 'week',
    country: 'RU',
    genre: 'dance',
    crawler: :radio_record_top100
  },
  {
    name: 'Rock Radio Top 100',
    link: 'http://www.radiorecord.ru/radio/top100/detail-flat.php?station=rock',
    tracks_count: 100,
    change_period: 'week',
    country: 'RU',
    genre: 'rock',
    crawler: :radio_record_top100
  },
  {
    name: 'Pirate Station Top 100',
    link: 'http://www.radiorecord.ru/radio/top100/detail-flat.php?station=ps',
    tracks_count: 100,
    change_period: 'week',
    country: 'RU',
    genre: 'dance',
    crawler: :radio_record_top100
  },
  {
    name: 'Trancemission Top 100',
    link: 'http://www.radiorecord.ru/radio/top100/detail-flat.php?station=tm',
    tracks_count: 100,
    change_period: 'week',
    country: 'RU',
    genre: 'dance',
    crawler: :radio_record_top100
  },
  {
    name: 'Record Chill-Out Top 100',
    link: 'http://www.radiorecord.ru/radio/top100/detail-flat.php?station=chil',
    tracks_count: 100,
    change_period: 'week',
    country: 'RU',
    genre: 'dance',
    crawler: :radio_record_top100
  },
  {
    name: 'Record Trap Top 100',
    link: 'http://www.radiorecord.ru/radio/top100/detail-flat.php?station=trap',
    tracks_count: 100,
    change_period: 'week',
    country: 'RU',
    genre: 'dance',
    crawler: :radio_record_top100
  }
]

default_sources.each do |source_attrs|
  Source.find_or_initialize_by(name: source_attrs[:name]).update(source_attrs)
end

