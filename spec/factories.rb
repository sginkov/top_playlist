FactoryGirl.define do
  factory :song do
    sequence(:name) { |i| "name#{i}" }
    sequence(:artist) { |i| "artist#{i}" }
  end
end