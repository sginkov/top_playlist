require 'rails_helper'

describe Song, type: :model, slow: true do
  context '::remove_duplicates' do
    before do
      @song1 = create(:song)
      @song2 = create(:song)
      @song3 = create(:song)
    end

    it 'should not update nothing if song_ids is empty' do
      expect(Song.remove_duplicates(nil, nil)).to be_falsey
    end

    it 'should not update nothing if primary is empty' do
      expect(Song.remove_duplicates([@song1.id], nil)).to be_falsey
    end

    context 'valid params' do
      before do
        Song.remove_duplicates([@song1.id, @song2.id, @song3.id], @song3.id)
      end

      it 'should remove duplicates' do
        expect(Song.in(id:@song1.id)).to eq([])
        expect(Song.in(id:@song2.id)).to eq([])
      end

      it 'should not remove song3' do
        expect(Song.in(id:@song3.id)).to eq([@song3])
      end

      it 'should change song aliases of duplicates to song3' do
        (@song1.song_aliases + @song2.song_aliases).each do |s|
          expect(SongAlias.find_by(id: s.id).song).to eq(@song3)
        end
      end
    end
  end
end
