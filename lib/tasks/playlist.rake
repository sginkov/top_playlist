namespace :playlist do
  desc 'Generate playlist reports for active sources'
  task generate: :environment do
    Source.active.each do |s|
      PlaylistFetcher.perform_async(s.id.to_s)
    end
  end
end