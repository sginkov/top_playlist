namespace :song do
  desc 'Add source from playlist report to ranks'
  task update_primary_images_count: :environment do
    Song.all.each do |s|
      s.update_primary_images_count
    end
  end
end