namespace :ranks do
  desc 'Add source from playlist report to ranks'
  task update_sources: :environment do
    Rank.all.each do |r|
      r.update_attribute(:source, r.playlist_report.source)
    end
  end
end