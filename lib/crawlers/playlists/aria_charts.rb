module Crawlers
  module Playlists
    class AriaCharts  < BaseSource
      protected

      def artist_converter(artist)
        if artist
          artist.split('|').first.rstrip
        end
      end

      def converters
        {
          artist: method(:artist_converter)
        }
      end

      def container_selector
        'css=.chart-box .item'
      end

      def fields
        {
          rank: 'css=.position',
          artist: 'css=.col-6 p',
          name: 'css=.col-6 h3',
          image_path: 'xpath=.//div[contains(@class,"col-5")]/img/@src'
        }
      end
    end
  end
end