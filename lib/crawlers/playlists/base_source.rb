module Crawlers
  module Playlists
    class BaseSource
      PLAYLIST_COLLECTION_NAME = 'songs'

      def initialize(source)
        @source = source
      end

      def build_playlist(link, date)
        data = fetch_data(link)
        decorated_data = decorate_playlist(data)
        playlist_report = @source.playlist_reports.find_or_create_by(date: date, link: link)
        decorated_data[:songs].each do |song_attributes|
          key = Song.generate_key(song_attributes[:name], song_attributes[:artist])
          song_alias = SongAlias.where(key: key).first
          song_alias = build_song(song_attributes) unless song_alias
          build_rank(song_attributes, playlist_report, song_alias) if song_alias
        end
        playlist_report.check_status
      end

      protected

      def fetch_data(link)
        Fetchers::Playlist.fetch(link, {
          container: container_selector,
          fields: fields
        })
      end

      def build_rank(song_attributes, playlist_report, song_alias)
        Rank.find_or_create_by(value: song_attributes[:rank], playlist_report: playlist_report, song_alias: song_alias)
      end

      def build_song(song_attributes)
        song = Song.find_or_create_by(artist: song_attributes[:artist], name: song_attributes[:name])
        if song.valid?
          store_image(song, song_attributes)
          store_video_link(song, song_attributes)
          song.primary_song_alias
        end
      end

      def store_video_link(song, song_attributes)
        if song_attributes[:video_path]
          video_link = song.video_links.
            find_or_initialize_by(link: song_attributes[:video_path], source: @source)
          video_link.save!
        end
      rescue => ex
        Rails.logger.error(ex)
      end

      def store_image(song, song_attributes)
        if song_attributes[:image_path]
          image = song.images.
            find_or_initialize_by(link: song_attributes[:image_path], source: @source, file_from_link: true)
          image.save!
        end
      rescue => ex
        Rails.logger.error(ex)
      end

      def container_selector
        raise NotImplemented
      end

      def fields
        {}
      end

      def converters
        {}
      end

      def decorate_playlist(data)
        data[PLAYLIST_COLLECTION_NAME].each do |object|
          fields.each do |key, _|
            converter = converters[key]
            object[key] = converter.call(object[key]) if converter
          end
        end
        data
      end
    end
  end
end