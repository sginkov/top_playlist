module Crawlers
  module Playlists
    class DeutschlandRadiocharts < BaseSource
      protected

      def container_selector
        'css=table.chartTable tbody tr'
      end

      def fields
        {
          rank: 'xpath=.//td[2]/strong',
          artist: 'xpath=.//td[6]',
          name: 'xpath=.//td[5]'
        }
      end

    end
  end
end