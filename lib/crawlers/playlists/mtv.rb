module Crawlers
  module Playlists
    class Mtv  < BaseSource
      protected

      def container_selector
        'css=.video-wrapper .video-player .video-list .item-list ul li'
      end

      def fields
        {
          rank: 'css=.video-list-info .video-list-number',
          artist: 'css=.video-list-details .video-list-name',
          name: 'css=.video-list-details .video-list-title',
          image_path: 'xpath=.//div[@class="video-list-thumb"]/img/@src'
        }
      end
    end
  end
end