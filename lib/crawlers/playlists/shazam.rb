module Crawlers
  module Playlists
    class Shazam  < BaseSource
      protected

      def container_selector
        'css=.flexgrid__item--large'
      end

      def fields
        {
          rank: 'xpath=.//article[contains(@class, "ti__container")]/@data-chart-position',
          artist: 'xpath=.//div[contains(@class, "ti__details")]//p[contains(@class, "ti__artist")]/meta/@content',
          name: 'xpath=.//div[contains(@class, "ti__details")]//p[contains(@class, "ti__title")]/@content',
          image_path: 'xpath=.//div[contains(@class, "ti__cover-art")]//img/@src'
        }
      end
    end
  end
end