module Crawlers
  module Playlists
    class BbcRadio  < BaseSource
      protected

      def container_selector
        'css=#chtwrapper .cht-entries .cht-entry-wrapper'
      end

      def fields
        {
          rank: 'css=.cht-entry-position',
          artist: 'css=.cht-entry-artist a',
          name: 'css=.cht-entry-title',
          image_path: 'xpath=.//img[@class="cht-entry-image"]/@src'
        }
      end
    end
  end
end