module Crawlers
  module Playlists
    class LastFm  < BaseSource
      protected

      def decorate_name(name)
        if name
          name.split('–')[1].try(:lstrip)
        end
      end

      def decorate_artist(name)
        if name
          name.split('–')[0].try(:rstrip)
        end
      end

      def decorate_rank(rank)
        if rank
          rank.gsub('.', '')
        end
      end

      def converters
        {
          rank: method(:decorate_rank),
          artist: method(:decorate_artist),
          name: method(:decorate_name)
        }
      end

      def container_selector
        'css=.rankedChart ol li.rankItem'
      end

      def fields
        {
          rank: 'css=.rankItem-position',
          artist: 'css=.rankItem-title',
          name: 'css=.rankItem-title',
          image_path: 'xpath=.//img/@src'
        }
      end
    end
  end
end