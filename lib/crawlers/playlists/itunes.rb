module Crawlers
  module Playlists
    class Itunes  < BaseSource
      protected

      def rank_converter(rank)
        if rank
          rank.gsub('.', '')
        end
      end

      def converters
        {
          rank: method(:rank_converter)
        }
      end

      def container_selector
        'css=.chart-grid .section-content ul li'
      end

      def fields
        {
          rank: 'css=strong',
          artist: 'css=h4 a',
          name: 'css=h3 a',
          image_path: 'xpath=.//a/img/@src'
        }
      end
    end
  end
end