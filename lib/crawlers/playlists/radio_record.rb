module Crawlers
  module Playlists
    class RadioRecord  < BaseSource
      protected

      def container_selector
        'css=.all_audio article'
      end

      def fields
        {
          rank: 'css=.place .place-num',
          artist: 'css=.player .artist',
          name: 'css=.player .name'
        }
      end
    end
  end
end