module Crawlers
  module Playlists
    class Los40  < BaseSource
      protected

      def decorate_name(name)
        if name
          name.gsub!(' »','')
        end
      end

      def converters
        {
          artist: method(:decorate_name)
        }
      end

      def container_selector
        'css=div.lista40 .article'
      end

      def fields
        {
          rank: 'css=.posicion',
          artist: 'css=.info_grupo h4 a',
          name: 'xpath=.//div[@class="info_grupo"]//p[1]',
          image_path: 'xpath=.//div[@class="miniatura"]/img/@src'
        }
      end
    end
  end
end