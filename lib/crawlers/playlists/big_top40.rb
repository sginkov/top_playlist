module Crawlers
  module Playlists
    class BigTop40  < BaseSource
      protected

      def artist_converter(artist)
        if artist
          artist.gsub(/^by/, '').lstrip
        end
      end

      def name_converter(name)
        if name
          name.scan(/^(.*)/).first.first.rstrip.gsub(/by$/, '')
        end
      end

      def converters
        {
          artist: method(:artist_converter),
          name: method(:name_converter)
        }
      end

      def container_selector
        'css=ul.chart_list li'
      end

      def fields
        {
          rank: 'css=div.entry_details .chart_position',
          artist: 'css=div.entry_details h3 span',
          name: 'css=div.entry_details h3',
          image_path: 'xpath=.//div[@class="entry_details"]/a/img/@src'
        }
      end
    end
  end
end