module Crawlers
  module Playlists
    class MtvGermany  < BaseSource
      protected

      def container_selector
        'css=div.chart-container ul.video-collection li'
      end

      def fields
        {
          rank: 'css=div.chart-info div.chart-position',
          artist: 'css=div.info-wrapper h4 a',
          name: 'css=div.info-wrapper h3',
          image_path: 'xpath=.//div[@class="teaser-image"]/img/@src'
        }
      end
    end
  end
end