module Crawlers
  module Playlists
    class RadioRecordTop100  < BaseSource
      protected

      def container_selector
        'css=.top100_media'
      end

      def fields
        {
          rank: 'css=.top100_pos',
          artist: 'css=.top100_artist',
          name: 'css=.top100_title'
        }
      end
    end
  end
end