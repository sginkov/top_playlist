module Crawlers
  module Playlists
    class LoveRadio  < BaseSource
      protected

      def converters
        {
          name: method(:decorate_name),
          artist: method(:decorate_artist)
        }
      end

      def decorate_name(full_name)
        if full_name
          full_name.split(' - ')[1]
        end
      end

      def decorate_artist(full_name)
        if full_name
          full_name.split(' - ')[0]
        end
      end

      def container_selector
        'css=.biglove table tbody tr'
      end

      def fields
        {
          rank: 'xpath=.//td[1]',
          artist: 'xpath=.//td[4]',
          name: 'xpath=.//td[4]'
        }
      end
    end
  end
end