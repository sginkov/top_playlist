module Crawlers
  module Playlists
    class FranceRadioAirplay  < BaseSource
      protected

      def name_converter(name)
        if name
          name.split("\t").last
        end
      end

      def converters
        {
          name: method(:name_converter)
        }
      end

      def container_selector
        'css=#fondcharts .line'
      end

      def fields
        {
          rank: 'css=.style3',
          artist: 'css=.style5d strong',
          name: 'css=.style5d'
        }
      end
    end
  end
end