module Crawlers
  module Playlists
    class MtvAsia  < BaseSource
      protected

      def container_selector
        'css=table.fullcharttable tr'
      end

      def fields
        {
          rank: 'xpath=.//td[@class="cawhite"][1]',
          artist: 'xpath=.//td[@class="cawhite"][6]',
          name: 'xpath=.//td[@class="cawhite"][5]'
        }
      end
    end
  end
end