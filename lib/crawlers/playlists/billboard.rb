module Crawlers
  module Playlists
    class Billboard  < BaseSource
      def decorate_playlist(data)
        decorated_data = super(data)
        decorated_data[PLAYLIST_COLLECTION_NAME].each do |song|
          song[:artist] ||= song[:artist2]
          song[:image_path] ||= song[:image_path2]
        end
        decorated_data
      end

      def container_selector
        'css=.chart-data article.chart-row'
      end

      def converters
        {
          image_path: ->(value){ Converters::StyleBackgroundImage.convert(value) }
        }
      end

      def fields
        {
          rank: 'css=.row-primary .row-rank .this-week',
          artist: 'css=.row-title h3 a',
          artist2: 'css=.row-title h3',
          name: 'css=.row-title h2',
          image_path: 'xpath=.//div[@class="row-image"]/@style',
          image_path2: 'xpath=.//div[@class="row-image"]/@data-imagesrc',
          video_path: 'xpath=.//div[@class="row-watch"]/a/@href',
        }
      end
    end
  end
end