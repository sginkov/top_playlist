module Crawlers
  module Playlists
    class EuropaPlus  < BaseSource
      protected

      def converters
        {
          image_path: ->(value){ Converters::ImagePathWithoutProtocol.convert(value) }
        }
      end

      def container_selector
        'css=ul.songs-list.players-list li .songs-holder'
      end

      def fields
        {
          rank: 'css=.number',
          artist: 'css=.jp-title .title',
          name: 'css=.jp-title span',
          image_path: 'xpath=.//div[@class="img-holder"]/a/img/@src'
        }
      end
    end
  end
end