module Crawlers
  module Playlists
    class RadioEnergy  < BaseSource
      protected

      def converters
        {
          rank: method(:decorate_rank),
          image_path: method(:image_full_path)
        }
      end

      def decorate_rank(rank)
        if rank
          rank.gsub('r', '')
        end
      end

      def image_full_path(image_path)
        Crawlers::UrlHelper.resource_absolute_url(@source.link, image_path) if image_path
      end

      def container_selector
        'css=.hot30_list ul li'
      end

      def fields
        {
          rank: 'xpath=.//span[1]/@class',
          artist: 'css=.text span',
          name: 'css=.text a',
          image_path: 'xpath=.//div[@class="image"]/img/@src'
        }
      end
    end
  end
end