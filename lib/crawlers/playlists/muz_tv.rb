module Crawlers
  module Playlists
    class MuzTv  < BaseSource
      protected

      def image_full_path(image_path)
        Crawlers::UrlHelper.resource_absolute_url(@source.link, image_path) if image_path
      end

      def converters
        {
          image_path: method(:image_full_path)
        }
      end

      def container_selector
        'css=.x-results .b-chartvoting_item .b-wrapper'
      end

      def fields
        {
          rank: 'css=.b-chartvoting_numb',
          artist: 'css=.b-title',
          name: 'css=.b-text',
          image_path: 'xpath=.//div[@class="b-media-frame"]/img/@src'
        }
      end
    end
  end
end