module Crawlers
  module Playlists
    class At40 < BaseSource
      protected

      def name_converter(name)
        if name
          name.scan(/^(.*)/).last.first.lstrip
        end
      end

      def converters
        {
          name: method(:name_converter)
        }
      end

      def container_selector
        'css=table.charttableint tr'
      end

      def fields
        {
          rank: 'xpath=.//td[1]//span',
          artist: 'xpath=.//td[4]//a',
          name: 'xpath=.//td[4]',
          image_path: 'xpath=.//td[@class="chartcover"]//img/@src'
        }
      end
    end
  end
end