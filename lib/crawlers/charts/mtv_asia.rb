module Crawlers
  module Charts
    class MtvAsia < BaseSource
      def find_charts(count = 1)
        chart = super(1).first
        chart[:link] = @source.link
        [chart]
      end

      protected

      def container_selector
        'xpath=//*[contains(@class, "ca_container")]//table//table//table[last()]//tr[last()]//td[last()]/text()'
      end

      def fields
        {
          date: 'xpath=.'
        }
      end
    end
  end
end