module Crawlers
  module Charts
    class RadioRecord < BaseSource
      def find_charts(count = 1)
        chart = super(1).first
        chart[:link] = @source.link
        [chart]
      end

      protected

      def container_selector
        'css=#page-superchart'
      end

      def fields
        {
          date: 'css=.supercart-title .sc-date'
        }
      end
    end
  end
end