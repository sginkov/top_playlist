module Crawlers
  module Charts
    class LoveRadio < BaseSource
      def find_charts(count = 1)
        chart = super(1).first
        chart[:link] = @source.link
        [chart]
      end

      protected

      def container_selector
        'css=.left_sidebar'
      end

      def fields
        {
          date: 'css=.left_block_headline_standalone a'
        }
      end
    end
  end
end