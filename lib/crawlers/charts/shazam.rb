module Crawlers
  module Charts
    class Shazam < PeriodSource
      protected

      def release_at
        Date.today.beginning_of_week(:saturday)
      end
    end
  end
end