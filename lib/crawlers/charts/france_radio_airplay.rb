module Crawlers
  module Charts
    class FranceRadioAirplay < BaseSource
      def find_charts(count = 1)
        chart = super(1).first
        chart[:link] = @source.link
        [chart]
      end

      protected

      def container_selector
        'css=#soustitre'
      end

      def fields
        {
          date: 'css=b'
        }
      end
    end
  end
end