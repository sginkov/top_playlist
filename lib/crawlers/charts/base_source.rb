module Crawlers
  module Charts
    class BaseSource
      CHART_COLLECTION_NAME = 'charts'
      WEEK_PERIOD = 7

      def initialize(source)
        @source = source
      end

      def find_charts(count=1)
        data = Fetchers::AvailableCharts.fetch(@source.link, {
          container: container_selector,
          fields: fields
        })
        charts = data[CHART_COLLECTION_NAME]
        ((charts || [])[0...count]).map do |c|
          decorate(c)
        end
      end

      protected

      def container_selector
        raise NotImplemented
      end

      def fields
        {}
      end

      def converters
        {
          date: ->(date_string) { Converters::Date.convert(date_string) }
        }
      end

      def decorate(chart)
        converters.each do |field, converter|
          chart[field] = converter.call(chart[field])
        end
        chart
      end
    end
  end
end