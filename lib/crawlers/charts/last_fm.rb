module Crawlers
  module Charts
    class LastFm < BaseSource
      def find_charts(count)
        chart = super(1).first
        chart[:link] = @source.link
        [chart]
      end

      protected

      def container_selector
        'xpath=//*[contains(@class, "weekpicker")]//strong/text()'
      end

      def fields
        {
          date: 'xpath=.'
        }
      end
    end
  end
end