module Crawlers
  module Charts
    class Mtv < BaseSource
      def find_charts(count)
        chart = super(1).first
        chart[:link] = @source.link
        [chart]
      end

      protected

      def converters
        {
          date: method(:parse_date)
        }
      end

      def parse_date(value)
        if value
          date = value.scan(/.*: (.*)/).try(:first).try(:first)
          Date.parse(date)
        end
      end

      def container_selector
        'css=.video-wrapper .video-header hgroup'
      end

      def fields
        {
          date: 'css=h2'
        }
      end
    end
  end
end