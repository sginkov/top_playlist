module Crawlers
  module Charts
    class Billboard < BaseSource
      def find_charts(count)
        chart = super(1).first
        if chart
          charts = []
          count.times do |i|
            date = chart[:date] - (WEEK_PERIOD*(count - i - 1)).days
            link = File.join(@source.link, '/', date.to_s)
            charts << {date: date, link: link}
          end
          charts
        end
      end

      protected

      def container_selector
        'css=#chart-nav'
      end

      def fields
        {
          date: 'xpath=.//time/@datetime'
        }
      end
    end
  end
end