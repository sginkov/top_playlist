module Crawlers
  module Charts
    class RadioRecordTop100 < PeriodSource
      protected

      def release_at
        Date.today.beginning_of_week(:sunday)
      end
    end
  end
end