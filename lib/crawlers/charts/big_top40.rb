module Crawlers
  module Charts
    class BigTop40 < BaseSource
      protected

      def converters
        {
          date: method(:parse_date),
          link: method(:full_path)
        }
      end

      def parse_date(value)
        if value
          date = value.scan(/Chart for week ending (.*)/).try(:first).try(:first)
          Date.parse(date)
        end
      end

      def full_path(link)
        Crawlers::UrlHelper.resource_absolute_url(@source.link, link) if link
      end

      def container_selector
        'css=ul.recent_charts li'
      end

      def fields
        {
          date: 'xpath=.//h3//a/span',
          link: 'xpath=.//h3//a/@href'
        }
      end
    end
  end
end