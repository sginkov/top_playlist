module Crawlers
  module Charts
    class MuzTv < BaseSource
      protected

      def converters
        {
          date: method(:parse_date),
          link: method(:full_path)
        }
      end

      def parse_date(value)
        if value
          date = value.scan(/Выпуск от (.*)/).try(:first).try(:first)
          Date.parse(date)
        end
      end

      def full_path(link)
        Crawlers::UrlHelper.resource_absolute_url(@source.link, link) if link
      end

      def container_selector
        'xpath=//*[contains(@class, "x-results-container")]'
      end

      def fields
        {
          date: 'xpath=.//a[contains(@class,"b-chartvoting_archive_subitem_url")]',
          link: 'xpath=.//a[contains(@class,"b-chartvoting_archive_subitem_url")]/@href'
        }
      end
    end
  end
end