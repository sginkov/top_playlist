module Crawlers
  module Charts
    class PeriodSource < BaseSource
      def find_charts(count)
        [
          { link: @source.link, date: release_at }
        ]
      end

      protected

      def release_at
        Date.today.beginning_of_week(:sunday)
      end
    end
  end
end