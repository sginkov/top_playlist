module Crawlers
  module Charts
    class At40 < BaseSource
      protected

      def converters
        {
          date: ->(value){ Converters::Date.convert(value) },
          link: method(:full_path)
        }
      end

      def full_path(link)
        Crawlers::UrlHelper.resource_absolute_url(@source.link, link) if link
      end

      def container_selector
        'css=#categories table'
      end

      def fields
        {
          date: 'xpath=.//tr[1]//td[2]//time//a',
          link: 'xpath=.//tr[1]//td[2]//time//a/@href'
        }
      end
    end
  end
end