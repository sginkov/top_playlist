module Crawlers
  module Charts
    class AriaCharts < BaseSource
      def find_charts(count = 1)
        chart = super(1).first
        chart[:link] = @source.link
        [chart]
      end

      protected

      def container_selector
        'css=#two-columns'
      end

      def fields
        {
          date: 'css=.heading-2'
        }
      end
    end
  end
end