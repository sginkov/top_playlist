module Crawlers
  module Charts
    class BbcRadio < BaseSource
      def find_charts(count = 1)
        chart = super(1).first
        chart[:link] = @source.link
        [chart]
      end

      protected

      def container_selector
        'css=#chtwrapper'
      end

      def fields
        {
          date: 'css=.cht-date'
        }
      end
    end
  end
end