module Crawlers
  module Charts
    class Los40 < BaseSource
      def find_charts(count)
        chart = super(1).first
        chart[:link] = @source.link
        [chart]
      end

      protected

      def converters
        {
          date: method(:parse_date)
        }
      end

      def parse_date(value)
        if value
          date = value.scan(/^LISTA DEL (.*)/).try(:first).try(:first)
          Date.parse(date)
        end
      end

      def container_selector
        'css=div.lista40'
      end

      def fields
        {
          date: 'css=h3'
        }
      end
    end
  end
end