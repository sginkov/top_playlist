require 'uri'

module Crawlers
  module UrlHelper
    extend self

    def resource_absolute_url(url, path)
      uri = URI.parse(url)
      uri.path  = path
      uri.query = nil
      uri.to_s
    end
  end
end