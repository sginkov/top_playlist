module Converters
  module Date
    extend self

    def convert(value)
      ::Date.parse(value) if value
    end
  end
end