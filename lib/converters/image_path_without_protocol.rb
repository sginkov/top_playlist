module Converters
  module ImagePathWithoutProtocol
    extend self

    def convert(value)
      value.gsub(/^\/\//, 'http://') if value
    end
  end
end