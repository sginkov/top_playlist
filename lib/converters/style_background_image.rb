module Converters
  module StyleBackgroundImage
    extend self

    def convert(value)
      value.scan(/url\((.*)\)/).try(:first).try(:first) if value
    end
  end
end