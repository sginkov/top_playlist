module Fetchers
  class Playlist
    class << self
      def fetch(url, selectors)
        Wombat.crawl do
          path url

          songs(selectors[:container], :iterator) do
            selectors[:fields].each do |field_name, selector|
              send(field_name, selector)
            end
          end
        end.with_indifferent_access
      end
    end
  end
end

