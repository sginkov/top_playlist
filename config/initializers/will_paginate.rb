# monkeypatch
if defined?(WillPaginate)
  module WillPaginate
    class Collection
      alias_method :per, :per_page
      alias_method :limit_value, :per_page
      alias_method :num_pages, :total_pages
      alias_method :total_count, :count
    end
  end
end