require 'redis'

file_path = Rails.root.join('config', 'redis.yml')

if File.exists?(file_path)
  r = YAML.load_file(file_path)[Rails.env.to_s].symbolize_keys
  Sidekiq.configure_client do |config|
    config.redis = { url: "redis://#{r[:server]}:#{r[:port]}/#{r[:db_num]}/", namespace: r[:namespace]  }
  end

  Sidekiq.configure_server do |config|
    config.redis = { url: "redis://#{r[:server]}:#{r[:port]}/#{r[:db_num]}/", namespace: r[:namespace]  }
  end
end