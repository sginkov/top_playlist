file_path = Rails.root.join('config', 'elasticsearch.yml')

if File.exists?(file_path)
  config = YAML.load_file(file_path)[Rails.env.to_s].symbolize_keys
  Elasticsearch::Model.client = Elasticsearch::Client.new(config)
end
