module ActiveAdminResource
  extend ActiveSupport::Concern

  module ClassMethods
    ACTIVE_ADMIN_HIDDEN_COLUMNS = %w(_id _type)
    ACTIVE_ADMIN_COLUMN_TYPES   = { Bignum => :integer, Array => :string }

    def quote_column_name(column)
      column
    end

    def quoted_table_name
      collection.name
    end

    def content_columns
      @content_columns ||= begin
        fields.map do |name, field|
          next if ACTIVE_ADMIN_HIDDEN_COLUMNS.include?(name)
          OpenStruct.new.tap do |c|
            c.name = field.name
            c.type = ACTIVE_ADMIN_COLUMN_TYPES[field.type] || field.type.to_s.downcase.to_sym
          end
        end.compact
      end
    end

    def reflections
      {}
    end

    def connection
      scoped
    end

    def find_by_id(id)
      where(id: id).first
    end

    def primary_key
      :id
    end

    def table
      content_columns.inject({}.with_indifferent_access) do |memo, column|
        column_name = column.name
        memo[column_name] = ::Ransack::Adapters::Mongoid::Attributes::Attribute.new(self.criteria, column_name)
        memo
      end
    end
  end
end

Mongoid::Document.include(ActiveAdminResource)

module ActiveAdmin
  class OrderClause
    def to_sql(active_admin_config)
      [@column, @op, ' ', @order].compact.join
    end
  end
end

module ActiveAdmin
  module Helpers
    module Collection
      def collection_size(c = collection)
        c.count
      end
    end
  end
end



