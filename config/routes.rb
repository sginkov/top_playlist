require 'sidekiq/web'

TopPlaylist::Application.routes.draw do
  mount Ckeditor::Engine => '/ckeditor'
  devise_for :admin_users, ActiveAdmin::Devise.config
  ActiveAdmin.routes(self)

  authenticate :admin_user do
    mount Sidekiq::Web => '/sidekiq'
  end

  mount Client::API => '/'

  get 'api/docs' => 'api#docs'
end
