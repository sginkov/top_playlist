module Client
  module V1
    class UserInfo < Grape::API
      version 'v1', using: :header, vendor: 'client'
      format :json
      prefix :api

      helpers Client::Helpers::Collection
      helpers Client::Helpers::Authorization

      resource :user_info do

        params do
          use :authorization
        end
        route_param :uid do
          get 'get_votes' do
            votes = current_user.votes
            response_body(votes.count, votes)
          end

          params do
            requires :voteable_id, type: String, desc: 'UID'
            requires :voteable_type, type: String, desc: 'Type'
            requires :status, type: Boolean, desc: 'true/false'
          end
          post 'set_vote' do
            if params[:status]
              current_user.votes.find_or_create_by(
                voteable_id: BSON::ObjectId.from_string(params[:voteable_id]),
                voteable_type: params[:voteable_type])
            else
              current_user.votes.find_by(voteable_id: params[:voteable_id],
                voteable_type: params[:voteable_type]).destroy
            end
          end
        end
      end
    end
  end
end