module Client
  module V1
    class Sources < Grape::API
      version 'v1', using: :header, vendor: 'client'
      format :json
      prefix :api

      helpers Client::Helpers::Collection

      resource :primary_sources do
        desc 'Get primary sources with latest ranks'

        get do
          scope = Source.active.primary
          collection_data(PrimarySourceDecorator.decorate_collection(scope))
        end
      end

      resource :sources do
        desc 'Get sources'

        params do
          use :search, :pagination, :ordering
        end

        get do
          scope = Source.active
          if params[:q]
            scope = scope.search(name_cont: params[:q]).result
          end
          collection_data(scope) do |s|
            SourceDecorator.decorate_collection(s)
          end
        end

        desc 'Get source'
        params do
          requires :id, type: String, desc: 'Source ID'
        end
        route_param :id do
          get do
            SourceDecorator.new(Source.active.find(params[:id]))
          end

          resource :latest_report do
            desc 'Get latest playlist report'
            get do
              @source = Source.active.find(params[:id])
              @source.playlist_reports.first
            end
          end

          resource :ranks do
            desc 'Get latest ranks'

            params do
              use :pagination, :ordering, :search
            end
            get do
              @source = Source.active.find(params[:id])
              @report = @source.playlist_reports.first
              sources = @report.ranks
              if params[:q]
                sources = sources.search(full_text_search:params[:q]).result
              end
              collection_data(sources)
            end
          end
        end
      end
    end
  end
end