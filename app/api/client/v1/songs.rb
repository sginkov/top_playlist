module Client
  module V1
    class Songs < Grape::API
      version 'v1', using: :header, vendor: 'client'
      format :json
      prefix :api

      helpers Client::Helpers::Collection

      resource :songs do
        desc 'Get songs'

        params do
          use :search, :pagination, :ordering
        end

        get do
          scope = Song.active
          if params[:q]
            scope = scope.search(name_or_artist_cont: params[:q]).result
          end
          collection_data(scope)
        end

        params do
          use :search, :pagination, :ordering
          optional :duration, type: String, desc: 'Created at duration'
        end

        get :newest do
          scope = Song.newest(params[:duration].to_i)
          if params[:q]
            scope = scope.search(name_or_artist_cont: params[:q]).result
          end
          collection_data(scope)
        end

        desc 'Get song'
        params do
          requires :id, type: String, desc: 'Song ID'
        end
        route_param :id do
          get do
            Song.find(params[:id])
          end
        end
      end
    end
  end
end