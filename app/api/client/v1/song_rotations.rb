module Client
  module V1
    class SongRotations < Grape::API
      version 'v1', using: :header, vendor: 'client'
      format :json
      prefix :api

      helpers Client::Helpers::Collection

      resource :song_rotations do
        desc 'Get song for sources'

        params do
          optional :source_ids, type: Array
          optional :genres, type: Array
          use :pagination, :ordering
        end

        get do
          search = Source.active.search(id_in: params[:source_ids], genre_in: params[:genres])
          songs = Search::Mongoid::Song.new(Date.current, search.result).
            search(params[:sort_by] || 'weight', params[:sort_order] || 'desc')
          scope = paginate_collection(songs)
          response_body(songs.count, scope)
        end
      end
    end
  end
end