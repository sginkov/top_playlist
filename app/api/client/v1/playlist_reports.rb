module Client
  module V1
    class PlaylistReports < Grape::API
      version 'v1', using: :header, vendor: 'client'
      format :json
      prefix :api

      helpers Client::Helpers::Collection

      resource :playlist_reports do
        desc 'Get playlist reports'

        params do
          use :pagination, :ordering
          optional :source_id, type: String
        end

        before do
          @scope = if params[:source_id]
            Source.find(params[:source_id]).playlist_reports.for_status_success
          else
            PlaylistReport.for_status_success
          end
        end

        get do
          sources =  @scope
          collection_data(sources)
        end

        desc 'Get playlist report'
        params do
          requires :id, type: String, desc: 'Playlist Report ID'
        end
        route_param :id do
          before do
            @report = PlaylistReport.for_status_success.find(params[:id])
          end

          get do
            @report
          end

          params do
            use :pagination, :ordering, :search
          end
          get :list do
            sources = @report.ranks
            if params[:q]
              sources = sources.search(full_text_search:params[:q]).result
            end
            collection_data(sources)
          end
        end
      end
    end
  end
end