require 'vkontakte_api'

module Client
  module Helpers
    module Authorization
      extend Grape::API::Helpers

      PROVIDERS = {
        vk: VkontakteApi::Client
      }.with_indifferent_access

      params :authorization do
        optional :uid, type: String, desc: 'User id'
        optional :provider, type: String, desc: 'Provider Name'
      end

      def current_user
        @current_user ||= begin
          User.find_or_create_by(uid: params[:uid], provider: params[:provider])
        end
      rescue => ex
        Rails.logger.error(ex)
        nil
      end
    end
  end
end