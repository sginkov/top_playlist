module Client
  module Helpers
    module Collection
      extend Grape::API::Helpers

      params :search do
        optional :q, type: String, desc: 'Search value'
      end

      params :ordering do
        optional :sort_by, type: String
        optional :sort_order, type: String
      end

      params :pagination do
        optional :page, type: Integer
        optional :per_page, type: Integer
      end

      def order_collection(scope)
        collection = scope
        if params[:sort_by] && params[:sort_order]
          collection = collection.order_by(params[:sort_by] => params[:sort_order])
        end
        collection
      end

      def paginate_collection(scope)
        collection = scope
        if params[:page] || params[:per_page]
          params[:per_page] ||= 10
          collection = collection.paginate(page: params[:page], per_page: params[:per_page])
        end
        collection
      end

      def response_body(total_count, collection)
        {
          _metadata:{
            total_count: total_count,
            sort_by: params[:sort_by],
            sort_order: params[:sort_order],
            per_page: params[:per_page],
            page: params[:page]
          },
          entities: collection
        }
      end

      def collection_data(scope)
        # Don't change method order! pagination should be the last
        c = order_collection(scope)
        c = paginate_collection(c)
        c = yield(c) if block_given?
        response_body(scope.count, c)
      end
    end
  end
end