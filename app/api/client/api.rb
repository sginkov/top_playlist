require 'grape-swagger'

module Client
  class API < Grape::API
    prefix :api

    helpers Client::Helpers::Collection
    helpers Client::Helpers::Authorization

    mount Client::V1::Sources
    mount Client::V1::PlaylistReports
    mount Client::V1::SongRotations
    mount Client::V1::Songs
    mount Client::V1::UserInfo

    add_swagger_documentation
  end
end