require 'will_paginate/array'

SongRotation = Struct.new(:name, :artist, :weight)

ActiveAdmin.register SongRotation, as: 'Songs Rotation' do
  menu label: 'Songs Rotation'
  actions :index
  config.batch_actions = false

  filter :id_in, label: 'Sources', as: :select, collection: ->(){ Source.active } , multiple: true

  controller do
    def find_collection
      @search = Source.active.search(params[:q])
      @songs = Search::Mongoid::Song.new(Date.current, @search.result).
        search('weight', 'desc').
        paginate(page: params[:page])
    end
  end

  index do
    render 'index', context: self, sortable: false
  end
end