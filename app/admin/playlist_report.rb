ActiveAdmin.register PlaylistReport do
  actions :index, :moderate, :show, :destroy
  batch_actions = false

  scope :all, default: true
  scope :for_status_moderating
  scope :for_status_failed

  member_action :moderate, method: :get do
    r = PlaylistReport.find(params[:id])
    r.moderate
    redirect_to({action: :show}, notice: 'Moderated!')
  end

  batch_action :moderate do |selection|
    PlaylistReport.find(selection).each do |r|
      r.moderate
    end
    redirect_to :back
  end

  action_item only: :show do
    link_to('Moderate', moderate_admin_playlist_report_path(playlist_report)) if playlist_report.moderating?
  end

  action_item only: :show do
    link_to('Playlist', admin_playlist_report_ranks_path(playlist_report))
  end

  index do
    selectable_column
    column :date
    column :source do |playlist|
      source = playlist.source
      if source
        link_to(source.name, admin_source_path(source))
      end
    end
    column :link do |playlist|
      link_to(playlist.link, playlist.link, target: '_blank')
    end
    column :created_at
    column(:status) do |playlist|
      class_hash = {
        moderating: 'warning',
        success: 'ok'
      }.with_indifferent_access
      status_tag(playlist.status, class_hash[playlist.status])
    end

    actions do |playlist|
      link_to('Playlist', admin_playlist_report_ranks_path(playlist))
    end
  end

  show do |playlist|
    attributes_table do
      row :date
      row :link
      row :status do
        class_hash = {
          moderating: 'warning',
          success: 'ok'
        }.with_indifferent_access
        status_tag(playlist.status, class_hash[playlist.status])
      end
      row :source
      row :rating do
        link_to('Playlist', admin_playlist_report_ranks_path(playlist))
      end
    end
  end
end
