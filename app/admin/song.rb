ActiveAdmin.register Song do
  actions :index, :show, :edit, :update
  config.sort_order = 'name_asc'

  permit_params :name, :artist, :status, :custom_search, :custom_search_query

  scope :all, default: true
  scope :active
  scope :inactive
  scope :edit_required

  filter :name_or_artist_contains, label: I18n.t('song.filters.name_or_artist')

  decorate_with SongDuplicateDecorator

  sidebar :actions, only: [:index] do
    ul do
      li render('check_dialog')
    end
  end

  sidebar :multimedia, only: [:show, :edit] do
    ul do
      li link_to I18n.t('song.links.images'), admin_song_images_path(song)
      li link_to I18n.t('song.links.video_links'), admin_song_video_links_path(song)
    end
  end


  batch_action :activate do |selection|
    Song.find(selection).each do |song|
      song.activate
    end
    redirect_to :back
  end

  batch_action :deactivate do |selection|
    Song.find(selection).each do |song|
      song.deactivate
    end
    redirect_to :back
  end

  index do
    selectable_column
    column :image do |s|
      image = s.primary_image
      if image
        image_tag(image.file.thumb.url)
      end
    end
    column :artist
    column :name
    column(:status) do |s|
      s.status? ? status_tag(I18n.t('song.statuses.active'), :ok) : status_tag(I18n.t('song.statuses.inactive'))
    end
    column :primary_video do |s|
      v = s.primary_video_link
      if v
        link_to(v.link, v.link, target: '_blank')
      end
    end

    actions
  end

  collection_action :remove_duplicates, method: :post do
    Song.remove_duplicates(params[:song_ids], params[:primary_id])
  end

  show do |song|
    attributes_table do
      row :primary_image do |s|
        image = s.primary_image
        if image
          image_tag(image.file.thumb.url)
        end
      end
      row :artist
      row :name
      row :status do |s|
        s.status? ? status_tag(I18n.t('song.statuses.active'), :ok) : status_tag(I18n.t('song.statuses.inactive'))
      end
      row :custom_search do |s|
        s.custom_search? ? status_tag('On', :ok) : status_tag('Off')
      end
      row :custom_search_query
      row :primary_video do |s|
        v = s.primary_video_link
        if v
          link_to(v.link, v.link, target: '_blank')
        end
      end
    end
  end

  form(html: { multipart: true }) do |f|
    f.inputs 'Song Details' do
      f.input :name
      f.input :artist
      f.input :status, as: :boolean, label: 'Active'
      f.input :custom_search, as: :boolean
      f.input :custom_search_query
    end
    f.actions
  end
end
