require 'will_paginate/array'

ActiveAdmin.register VideoLink do
  menu label: 'Song Videos'
  navigation_menu :project
  config.filters = false

  belongs_to :song

  permit_params :link, :primary

  index do
    selectable_column
    column :source do |v|
      v.source.try(:name)
    end
    column :link do |v|
      link_to(v.link, v.link, target: '_blank')
    end
    column :primary do |image|
      image.primary? ? status_tag('Primary', :ok) : status_tag('Secondary')
    end
    actions
  end

  show do
    attributes_table do
      row :source do |v|
        v.source.try(:name)
      end
      row :link do |v|
        link_to(v.link, v.link, target: '_blank')
      end
      row :primary do |image|
        image.primary? ? status_tag('Primary', :ok) : status_tag('Secondary')
      end
    end
  end

  form do |f|
    f.inputs :details do
      f.input :link
      f.input :primary, as: :boolean
    end
    f.actions
  end
end