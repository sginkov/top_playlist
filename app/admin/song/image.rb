require 'will_paginate/array'

ActiveAdmin.register Image do
  menu label: 'Song Images'
  navigation_menu :project
  config.filters = false

  belongs_to :song, class_name: 'Song', polymorphic: true

  permit_params :link, :file, :primary, :file_from_link

  index do
    selectable_column
    column :source do |image|
      image.source.try(:name)
    end
    column :image do |image|
      image_tag(image.file.thumb.url)
    end
    column :link
    column :primary do |image|
      image.primary? ? status_tag('Primary', :ok) : status_tag('Secondary')
    end
    actions
  end

  show do
    attributes_table do
      row :source do |image|
        image.source.try(:name)
      end
      row :image do |image|
        image_tag(image.file.thumb.url)
      end
      row :link
      row :primary do |image|
        image.primary? ? status_tag('Primary', :ok) : status_tag('Secondary')
      end
    end
  end

  form(html: { multipart: true }) do |f|
    f.inputs :details do
      f.input :link
      f.input :file, as: :file
      f.input :primary, as: :boolean
      f.input :file_from_link, as: :boolean
    end
    f.actions
  end
end