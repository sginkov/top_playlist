ActiveAdmin.register_page "Dashboard" do

  menu priority: 1, label: proc{ I18n.t('active_admin.dashboard') }

  content title: proc{ I18n.t('active_admin.dashboard') } do
    columns do
      scope = PlaylistReport.for_status_failed
      column do
        panel "Failed Playlists (#{scope.count})" do
          render 'admin/playlist_reports/index', playlist_reports: scope.limit(10), context: self, sortable: false
        end
      end
    end

    columns do
      scope = PlaylistReport.for_status_moderating
      column do
        panel "Playlists for moderating (#{scope.count})" do
          render 'admin/playlist_reports/index', playlist_reports: scope.limit(10), context: self, sortable: false
        end
      end
    end

  end
end
