ActiveAdmin.register User do
  actions :index, :show
  config.sort_order = 'uid_asc'

  index do
    selectable_column

    column :uid
    column :provider
    actions
  end

  show do
    attributes_table do
      row :uid
      row :provider
    end
  end
end
