ActiveAdmin.register Source do
  permit_params :name, :link, :country, :region, :primary,  :image,
    :tracks_count, :change_period, :genre, :crawler, :status, description: [:en, :ru]

  config.sort_order = 'name_asc'
  filter :name
  filter :status, as: :select, collection: [['Active', true], ['Inactive', false]]

  index do
    selectable_column
    column :name
    column :link
    column :crawler
    column :region
    column :country
    column :tracks_count
    column :change_period
    column :genre
    column :ratio
    column(:status) do |s|
      s.status? ? status_tag('Active', :ok) : status_tag('Inactive')
    end
    column(:primary) do |s|
      s.primary? ? status_tag('Primary', :ok) : status_tag('Secondary')
    end

    actions
  end

  show do |s|
    attributes_table do
      row :name
      row :link
      row :crawler
      row :region
      row :country
      row :tracks_count
      row :change_period
      row :genre
      row :ratio
      row :status do |s|
        s.status? ? status_tag('Active', :ok) : status_tag('Inactive')
      end
      row :primary do |s|
        s.primary? ? status_tag('Active', :ok) : status_tag('Inactive')
      end
      row :image do |source|
        image_tag(source.image.thumb.url)
      end
    end
    attributes_table do
      row 'Description(En)' do |source|
        source.description.try(:[], :en)
      end
      row 'Description(Ru)' do |source|
        source.description.try(:[], :ru)
      end
    end
  end


  form(html: { multipart: true }) do |f|
    f.inputs 'Source Details' do
      f.input :name
      f.input :link
      f.input :crawler, as: :select, collection: Settings.crawlers
      f.input :region, as: :select, collection: Settings.regions
      f.input :country, as: :country, priority_countries: Settings.priority_countries, include_blank: true
      f.input :tracks_count
      f.input :change_period, as: :select, collection: Settings.change_periods
      f.input :genre, as: :select, collection: Settings.genres
      f.input :status, as: :boolean, label: 'Active'
      f.input :image, as: :file
      f.input :primary, as: :boolean
    end
    f.inputs for: :description do |ff|
      ff.input :en, as: :ckeditor, input_html: { value: f.resource.description.try(:[], :en) }
      ff.input :ru, as: :ckeditor, input_html: { value: f.resource.description.try(:[], :ru) }
    end
    f.actions
  end

end
