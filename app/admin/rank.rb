ActiveAdmin.register Rank do
  belongs_to :playlist_report
  actions :index
  config.sort_order = 'value_asc'
  config.batch_actions = false

  filter :full_text_search, as: :string, label: 'Song'

  index do
    column :rank do |r|
      r.value
    end
    column :song do |r|
      if (song = r.song)
        link_to(song.full_name, admin_song_path(song))
      end
    end
  end
end
