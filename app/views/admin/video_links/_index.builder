context.instance_eval  do
  if video_links.empty?
    empty_results_content = I18n.t('active_admin.pagination.empty', model: 'video links')
    insert_tag(view_factory.blank_slate, empty_results_content)
  else
    table_for(video_links, sortable: sortable, class: 'index_table') do
      column :source do |v|
        v.source.try(:name)
      end
      column :link do |v|
        link_to(v.link, v.link, target: '_blank')
      end
    end
  end
end