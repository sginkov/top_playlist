context.instance_eval  do
  if songs.empty?
    empty_results_content = I18n.t('active_admin.pagination.empty', model: 'songs')
    insert_tag(view_factory.blank_slate, empty_results_content)
  else
    table_for(songs, sortable: sortable, class: 'index_table') do
      column :image do |s|
        image = s.primary_image
        if image
          image_tag(image.file.thumb.url)
        end
      end
      column :song do |s|
        link_to(s.full_name, admin_song_path(s))
      end
      column :primary_video do |s|
        v = s.primary_video_link
        if v
          link_to(v.link, v.link, target: '_blank')
        end
      end
      column :weight
    end
  end
end