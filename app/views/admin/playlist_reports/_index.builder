context.instance_eval  do
  if playlist_reports.empty?
    empty_results_content = I18n.t('active_admin.pagination.empty', model: 'reports')
    insert_tag(view_factory.blank_slate, empty_results_content)
  else
    table_for(playlist_reports, sortable: sortable, class: 'index_table') do |st|
      column :date
      column :source do |playlist|
        source = playlist.source
        if source
          link_to(source.name, admin_source_path(source))
        end
      end
      column :link do |playlist|
        link_to(playlist.link, playlist.link, target: '_blank')
      end
      column :created_at
      column(:status) do |playlist|
        class_hash = {
          moderating: 'warning',
          success: 'ok'
        }.with_indifferent_access
        status_tag(playlist.status, class_hash[playlist.status])
      end
    end
  end
end