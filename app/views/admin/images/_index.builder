context.instance_eval  do
  if images.empty?
    empty_results_content = I18n.t('active_admin.pagination.empty', model: 'images')
    insert_tag(view_factory.blank_slate, empty_results_content)
  else
    table_for(images, sortable: sortable, class: 'index_table') do
      column :source do |image|
        image.source.try(:name)
      end
      column :image do |image|
        image_tag(image.file.thumb.url)
      end
      column :link do |image|
        image.link
      end
    end
  end
end