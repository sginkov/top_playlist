class SourceDecorator < Draper::Decorator
  delegate_all
  decorates :source

  def as_json(attributes = {})
    attrs = super(attributes.merge(methods: [:votes]))
    attrs['id'] = attrs['_id'].to_s
    attrs
  end
end