class SongDuplicateDecorator < Draper::Decorator
  delegate_all
  decorates :song

  def image_count
    object.images.count
  end

  def video_link_count
    object.video_links.count
  end

  def as_json(attributes = {})
    attrs = super(attributes.merge(methods: [:weight, :image_count, :video_link_count]))
    attrs['id'] = attrs['_id'].to_s
    attrs
  end
end
