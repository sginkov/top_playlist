class PrimarySourceDecorator < Draper::Decorator
  SONG_LIMIT = 5
  delegate_all
  decorates :source

  def latest_playlist_report
    object.playlist_reports.first
  end

  def ranks
    if latest_playlist_report
      latest_playlist_report.ranks.order_by_position.limit(SONG_LIMIT)
    end
  end

  def as_json(attributes = {})
    attrs = super(attributes.merge(methods: [:latest_playlist_report, :ranks, :votes]))
    attrs['id'] = attrs['_id'].to_s
    attrs
  end
end
