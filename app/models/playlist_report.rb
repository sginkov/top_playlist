class PlaylistReport
  include Mongoid::Document
  include Mongoid::Timestamps
  include Mongoid::Slug

  STATUSES = [:new, :success, :failed, :moderating]

  field :date, type: Date
  field :link, type: String
  field :status, type: String

  slug :date

  belongs_to :source
  has_many :ranks, dependent: :destroy

  validates :date, :source, presence: true

  default_scope ->{ order(date: :desc) }
  STATUSES.each do |status|
    scope "for_status_#{status}", ->{ where(status: status) }
  end
  scope :for_date, -> (date) { where(date: date)}
  scope :ready, -> (date) { for_status_success.for_date(date) }
  # search scopes
  scope :for_date_range, -> (date) { for_status_success.where(:date.gte => date-Settings.search_period.days, :date.lte => date+Settings.search_period.days) }
  scope :for_sources_with_date, -> (date, source_ids) { for_date_range(date).in(source_id: source_ids).desc(:date) }

  state_machine :status, initial: :new do
    event :check_status do
      transition all => :moderating, if: :success_checking
      transition all => :failed, unless: :success_checking
    end

    event :moderate do
      transition moderating: :success
    end
  end

  protected

  def success_checking
    self.ranks.count >= self.source.tracks_count
  end
end