class Image
  include Mongoid::Document

  field :link, type: String
  field :primary, type: Boolean, default: false

  belongs_to :imageable, polymorphic: true
  belongs_to :source

  before_save :download_file_from_link

  validates :link, :imageable, presence: true
  validates_uniqueness_of :link, scope: [ :imageable, :source ]

  mount_uploader :file, ImageUploader

  scope :primary, ->{ where(primary: true) }

  attr_accessor :file_from_link

  after_save :update_primary_count

  def as_json(attributes = {})
    super(attributes.merge(only: [ :file ]))
  end

  def file_from_link?
    [1, '1', true, 'true'].include?(file_from_link)
  end

  def download_file_from_link
    if file_from_link?
      self.file.download! self.link
      self.file.store!
    end
  end

  protected

  def update_primary_count
    imageable.update_primary_images_count if imageable.respond_to?(:update_primary_images_count)
  end
end