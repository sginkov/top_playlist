class Rank
  include Mongoid::Document

  field :value, type: Integer

  belongs_to :playlist_report
  belongs_to :song_alias
  # added for speeding up search
  belongs_to :source

  validates :value, :song_alias, :playlist_report, presence: true

  delegate :song, to: :song_alias, allow_nil: true

  before_save :set_source

  scope :full_text_search, ->(search) {
    song_ids = Song.search('artist_or_name_contains' => search).result.map(&:id)
    alias_ids = SongAlias.in(song_id: song_ids).map(&:id)
    self.in(song_alias_id: alias_ids)
  }

  scope :for_sources_with_date, ->(source_ids, date) {
    playlist_ids = PlaylistReport.for_sources_with_date(date, source_ids).
      group_by(&:source_id).map{|k,v| v.first.try(:id)}.compact
    Rank.in(playlist_report_id: playlist_ids).includes(:song_alias)
  }

  scope :order_by_position, ->(){ order(value: :asc) }

  def self.ransackable_scopes(auth_object = nil)
    [:full_text_search]
  end

  def as_json(options={})
    super options.merge(methods: :song)
  end

  protected

  def set_source
    self.source = playlist_report.source
  end
end