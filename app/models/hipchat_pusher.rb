require 'hipchat'

class HipchatPusher
  class << self
    def notify(message, options = {})
      if config.present?
        client(config['api_token'])[config['room']].send(config['username'], message, options)
      end
    end

    protected

    def client(api_token)
      @client ||= HipChat::Client.new(api_token)
    end

    def config
      @config ||= begin
        config_file_path = Rails.root.join(Dir.pwd, 'config/hipchat.yml')
        if File.exists?(config_file_path)
          c = YAML.load_file(config_file_path)
          c[Rails.env.to_s] || {}
        end
      end
    end
  end
end