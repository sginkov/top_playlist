class VideoLink
  include Mongoid::Document

  field :link, type: String
  field :primary, type: Boolean, default: false

  belongs_to :song
  belongs_to :source

  validates :link, :song, presence: true
  validates_uniqueness_of :link, scope: [ :song, :source ]

  scope :primary, ->{ where(primary: true) }

  def as_json(attributes = {})
    super(attributes.merge(only: [ :link ]))
  end
end