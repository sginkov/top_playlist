module WeightStrategies
  class Default
    TRACKS_COUNT_SHIFT = 13
    RATIO = 7
    INDEX_RATIO = 17

    class << self
      def normalize(rank, source, index=1)
        songs = (source.tracks_count+TRACKS_COUNT_SHIFT).to_f
        RATIO*source.ratio*(songs - rank)/(songs+index*INDEX_RATIO)
      end
    end
  end
end