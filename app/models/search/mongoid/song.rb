module Search
  module Mongoid
    class Song
      def initialize(date, sources)
        @date = date
        @sources = sources
      end

      def search(order_by, direction)
        playlist_ids = latest_playlists.map(&:id)
        song_ranks = search_songs(playlist_ids)
        songs =  send(:"order_by_#{order_by}", song_ranks, direction)
        songs
      end

      protected

      def source_hash
        @source_hash ||= array_to_hash(@sources)
      end

      def sorted_sources
        index = 0
        @sorted_sources ||= source_hash.values.sort{|a,b| b.priority <=> a.priority}.inject({}) do |memo, s|
          memo[s.id] = index
          index += 1
          memo
        end
      end

      def source_priority(source_id)
        sorted_sources[source_id]
      end

      def rank_weight(position, source, index)
        WeightStrategies::Default.normalize(position, source, index)
      end

      def sort_ranks(ranks)
        ranks.sort_by do |r|
          source_priority r.source_id
        end
      end

      def order_by_created_at(song_ranks, direction)
        songs = song_ranks.keys
        if direction == 'desc'
          songs.sort{|a,b| b.created_at <=> a.created_at }
        else
          songs.sort{|a,b| a.created_at <=> b.created_at }
        end
      end

      def order_by_weight(song_ranks, direction)
        songs = []
        song_ranks.each do |song, ranks|
          index = 0
          song.weight = sort_ranks(ranks).inject(0) do |memo, rank|
            source = source_hash[rank.source_id]
            memo += rank_weight(rank.value, source, index)
            index += 1
            memo
          end
          songs << song
        end
        if direction == 'desc'
          songs.sort{|a,b| b.weight <=> a.weight }
        else
          songs.sort{|a,b| a.weight <=> b.weight }
        end
      end

      def latest_playlists
        @playlists ||= PlaylistReport.
          for_sources_with_date(@date, @sources.map(&:id)).
          group_by(&:source_id).map { |k, v| v.first }.compact
      end

      def search_songs(playlist_ids)
        ranks = Rank.in(playlist_report_id: playlist_ids).group_by(&:song_alias_id)
        song_aliases = SongAlias.in(id: ranks.keys).to_a
        song_hash = array_to_hash(::Song.active.in(id: song_aliases.map(&:song_id)))
        song_aliases.inject({}) do |memo, song_alias|
          key = song_hash[song_alias.song_id]
          if key
            memo[key] ||= []
            memo[key] += ranks[song_alias.id]
          end
          memo
        end
      end

      private

      def array_to_hash(array)
        array.inject({}) do |memo, e|
          memo[e.id] = e
          memo
        end
      end
    end
  end
end