module Search
  module Elasticsearch
    class Song
      MAX_SIZE = 100000

      def initialize(date, sources)
        @date = date
        @sources = sources
      end

      def search
        playlist_ids = latest_playlists.map(&:id)
        results = search_songs(playlist_ids)
        songs = calculate_weight!(results)
        songs
      end

      protected

      def source_hash
        @source_hash ||= @sources.inject({}) do |memo, source|
          memo[source.id.to_s] = source
          memo
        end
      end

      def sorted_sources
        index = 0
        @sorted_sources ||= @sources.sort_by(&:priority).inject({}) do |memo, s|
          memo[s.id] = index
          index += 1
          memo
        end
      end

      def source_priority(source_id)
        sorted_sources[source_id]
      end

      def rank_weight(position, source, index)
        WeightStrategies::Default.normalize(position, source, index)
      end

      def sort_ranks(ranks)
        ranks.sort_by do |r|
          source_priority r._source[:playlist_report][:source][:_id]
        end
      end

      def calculate_weight!(results)
        songs = []
        results.each do |song, ranks|
          index = 0
          song[:weight] = sort_ranks(ranks).inject(0) do |memo, rank_result|
            _source = rank_result._source
            memo += rank_weight(_source[:value], source_hash[_source[:playlist_report][:source][:_id]], index)
            index += 1
            memo
          end
          songs << ::Song.new(song.to_hash)
        end
        songs.sort{|a,b| b.weight <=> a.weight }
      end

      def latest_playlists
        @playlists ||= PlaylistReport.
          for_sources_with_date(@date, @sources.map(&:id)).
          group_by(&:source_id).map { |k, v| v.first }.compact
      end

      def search_songs(playlist_ids)
        results = Rank.elastic_search(
          filter: {
            bool:{
              must:[
                { terms: { 'playlist_report._id' => playlist_ids } },
                { term: { 'song_alias.song.status' => true } }
              ]
            }
          },
          size: MAX_SIZE,
        ).results
        group_results(results)
      end

      def group_results(results)
        results.group_by{ |r| r[:_source][:song_alias][:song] }
      end
    end
  end
end