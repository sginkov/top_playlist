class User
  include Mongoid::Document
  include Mongoid::Timestamps

  field :provider, type: String
  field :uid, type: String

  has_many :votes, dependent: :destroy

  class << self
    def get_random_users(n)
      offset = 0
      cnt = User.count
      if n < cnt
        offset = rand(cnt-n)
      end
      User.skip(offset).limit(n)
    end
  end
end