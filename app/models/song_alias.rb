class SongAlias
  include Mongoid::Document
  field :key, type: String

  belongs_to :song
  has_many :ranks

  validates :key, presence: true
end