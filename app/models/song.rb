require 'base64'

class Song
  include Mongoid::Document
  include Mongoid::Timestamps

  field :name, type: String
  field :artist, type: String
  field :status, type: Boolean, default: false
  field :custom_search, type: Boolean, default: false
  field :custom_search_query, type: String
  field :primary_images_count, type: Integer, default: 0

  has_many :song_aliases
  has_many :images, as: :imageable, dependent: :destroy
  has_many :video_links, dependent: :destroy

  validates :name, presence: true

  after_save :create_song_alias

  scope :inactive, ->{ where(status: false) }
  scope :active, ->{ where(status: true) }
  scope :edit_required, -> { where(primary_images_count: 0) }
  scope :newest, -> (duration) {
    date = DateTime.current
    active.where(:created_at.gte => date-duration.days, :created_at.lte => date+duration.days)
  }

  attr_accessor :weight

  def activate
    update_attribute(:status, true)
  end

  def deactivate
    update_attribute(:status, false)
  end

  def primary_song_alias
    song_aliases.first
  end

  def rank_for_playlist(playlist_report)
    Rank.where(playlist_reports: playlist_report).in(key: song_aliases.map(&:key)).first.try(:value).to_i
  end

  def full_name
    [artist, name].join(' - ')
  end

  def primary_image
    images.primary.last
  end

  def primary_video_link
    video_links.primary.last
  end

  def search_query
    if self.custom_search?
      self.custom_search_query
    else
      self.generate_search_query
    end
  end

  def as_json(attributes = {})
    super(attributes.merge(methods: [:id, :weight, :primary_image, :primary_video_link, :search_query]))
  end

  def update_primary_images_count
    self.update_attribute(:primary_images_count, self.images.primary.count)
  end

  protected

  def generate_search_query
    [:name, :artist].inject('') do |memo, field|
      value = self.send(field) || ''
      memo << value.gsub(/(featuring|feat\.|feat|&|\(|\))/i, '') + ' '
      memo
    end
  end

  def create_song_alias
    key = Song.generate_key(self.name, self.artist)
    self.song_aliases.find_or_create_by(key: key)
  end

  class << self
    def remove_duplicates(song_ids, primary_id)
      if song_ids.present? && primary_id.present?
        primary = Song.find_by(id: primary_id)
        songs = Song.in(id: song_ids)
        songs.each do |s|
          s.song_aliases.each do |sa|
            sa.update(song: primary)
          end
          s.images.each do |img|
            img.update(imageable: primary)
          end
          s.video_links.each do |v|
            v.update(song: primary)
          end
          s.reload.destroy if s.song_aliases.count == 0
        end
        primary.activate
      end
    end

    def generate_key(name, artist)
      Base64.encode64([name, artist].join(''))
    end
  end
end