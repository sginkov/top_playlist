class Source
  include Mongoid::Document
  include Mongoid::Slug
  include PlaylistBuilder
  include VoteCounter

  field :name, type: String
  field :link, type: String
  field :region, type: String
  field :country, type: String
  field :tracks_count, type: Integer, default: 20
  field :change_period, type: String
  field :genre, type: String
  field :status, type: Boolean, default: true
  field :crawler, type: String
  field :ratio, type: Integer, default: 1
  field :description, localize: true
  field :primary, type: Boolean, default: false
  field :votes_count, type: Integer, default: 0

  slug :name

  validates :name, :link, :tracks_count, :change_period, :genre, :crawler, presence: true
  validates_uniqueness_of :name, :link

  has_many :playlist_reports
  has_many :votes, as: :voteable, dependent: :destroy

  scope :active, ->{ where(status: true) }
  scope :primary, ->{ where(primary: true) }
  default_scope ->{ order(votes_count: :desc, name: :asc) }

  attr_writer :priority

  mount_uploader :image, ImageUploader

  def priority
    votes_count
  end
end
