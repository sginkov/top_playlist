class Vote
  include Mongoid::Document
  include Mongoid::Timestamps

  belongs_to :voteable, polymorphic: true, counter_cache: :votes_count
  belongs_to :user
end