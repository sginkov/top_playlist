module PlaylistBuilder
  extend ActiveSupport::Concern

  def find_charts(count = 1)
    chart_crawler_class = "Crawlers::Charts::#{self.crawler.camelcase}".constantize
    chart_crawler_class.new(self).find_charts(count)
  end

  def build_playlists(count = 5)
    charts = find_charts(count)
    charts.each do |ch|
      build_playlist(ch[:link], ch[:date])
    end
  end

  def build_latest_playlist
    build_playlists(1)
  end

  def build_playlist(link, date)
    playlist_report = self.playlist_reports.ready(date).first
    unless playlist_report
      playlist_crawler_class = "Crawlers::Playlists::#{self.crawler.camelcase}".constantize
      playlist_crawler_class.new(self).build_playlist(link, date)
      HipchatPusher.notify("Build playlist '#{self.name}' report for #{date} from #{link}", color: 'green')
    end
  end
end
