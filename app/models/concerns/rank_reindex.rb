module RankReindex
  extend ActiveSupport::Concern

  included do
    after_update :reindex_ranks
  end

  def reindex_ranks
    song_alias_ids = self.song_aliases.map{|s| s.id.to_s }
    RankIndexer.perform_async(song_alias: song_alias_ids)
  end
end
