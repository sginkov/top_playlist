module VoteCounter
  extend ActiveSupport::Concern

  def set_votes(count)
    User.get_random_users(count).each do |user|
      user.votes.find_or_create_by(
        voteable_id: self.id,
        voteable_type: self.class.name
      )
    end
  end
end