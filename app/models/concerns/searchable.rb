module Searchable
  extend ActiveSupport::Concern

  included do
    include Elasticsearch::Model
    include Elasticsearch::Model::Callbacks
  end

  module ClassMethods
    def elastic_search(*args,&block)
      __elasticsearch__.search(*args, &block)
    end
  end
end