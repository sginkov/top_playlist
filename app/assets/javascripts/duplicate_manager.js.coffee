class window.DuplicateManager
  @options:
    dialogClass: ActiveAdminDialog
    selectors:
      duplicateContainer: '.songs_container tbody'
      filterForm: '.filter_form'
      collectionTmpl: '#songTmpl'
      noResultsTmpl: '#songNoResultsTmpl'
      actionsContainer:'.actions'

  constructor: (dialogSelector, options = {}) ->
    @options = $.extend({}, DuplicateManager.options, options)

    @dialog = $(dialogSelector)
    new @options.dialogClass(@dialog, {title: 'Duplicate Song Manager'})

    @filterForm = @dialog.find(@options.selectors.filterForm)
    @duplicateContainer = @dialog.find(@options.selectors.duplicateContainer)
    @duplicateContainer.html($(@options.selectors.noResultsTmpl).tmpl())

    @_bindFilterFormHandler();


  _bindFilterFormHandler: ->
    @filterForm.on('ajax:success', (event, data, xhr) =>
      @duplicateContainer.html("")
      @filterForm.find('input[type="text"]').removeAttr 'disabled'
      html = if data.length == 0
        @dialog.find(@options.selectors.actionsContainer).hide()
        $(@options.selectors.noResultsTmpl).tmpl()
      else
        @dialog.find(@options.selectors.actionsContainer).show()
        $(@options.selectors.collectionTmpl).tmpl(data)
      html.appendTo(@duplicateContainer)
    )
