class window.ActiveAdminDialog
  @options:
    modal: true
    dialogClass: 'active_admin_dialog'
    height: 500
    width: 600
    buttons:
      OK: ->
        $(@).dialog('close')
  constructor: (container, options = {}) ->
    opts = $.extend({}, ActiveAdminDialog.options, options)
    container.dialog(opts)

