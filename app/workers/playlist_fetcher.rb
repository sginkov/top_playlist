class PlaylistFetcher
  include Sidekiq::Worker
  sidekiq_options queue: 'playlist_reports', retry: 5

  def perform(source_id)
    source = Source.find_by(id: source_id)
    source.build_latest_playlist
  end
end