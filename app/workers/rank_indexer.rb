class RankIndexer
  include Sidekiq::Worker
  sidekiq_options queue: 'elasticsearch', retry: 5

  def perform(search_options = {})
    ranks = Rank.in(search_options)
    ranks.each do |r|
     r.__elasticsearch__.index_document
    end
  end
end